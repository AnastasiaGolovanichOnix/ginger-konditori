import { defineNuxtRouteMiddleware, navigateTo } from '#app';
import { useAuth } from '~/composables/firebase/useAuth';
import { RoutePaths } from '~/enums/RoutePaths';

export default defineNuxtRouteMiddleware(async (to, from) => {
    const { user, isLoading } = useAuth();

    if (isLoading.value) {
        return;
    }
    if (!user.value) {
        return navigateTo(RoutePaths.SIGN_IN);
    }
});