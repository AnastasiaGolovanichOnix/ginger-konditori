import { ref } from 'vue';
import {
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut,
    updateProfile,
    onAuthStateChanged,
    User,
    Unsubscribe
} from 'firebase/auth';
import { useNuxtApp } from '#app';

const user = ref<User | null>(null);
const error = ref<string | null>(null);
const isLoading = ref(true); // Add loading state

export const useAuth = () => {
    const { $auth } = useNuxtApp();
    let unsubscribe: Unsubscribe | null = null; // Store unsubscribe function

    const initializeAuth = () => {
        if (process.client && $auth) {
            unsubscribe = onAuthStateChanged($auth, (currentUser) => {
                user.value = currentUser;
                isLoading.value = false;
            });
        } else {
            isLoading.value = false; // No auth on server, set loading to false
        }
    };

    const cleanupAuth = () => {
        if (unsubscribe) {
            unsubscribe();
            unsubscribe = null;
        }
    };

    const signUp = async (email: string, password: string, displayName: string) => {
        if (!process.client || !$auth) throw new Error('Authentication not available');
        try {
            const userCredential = await createUserWithEmailAndPassword($auth, email, password);
            await updateProfile(userCredential.user, { displayName });
            user.value = { ...userCredential.user, displayName }; // Update local user state
            error.value = null;
            return userCredential.user;
        } catch (err: any) {
            error.value = err.message || 'Sign up failed';
            throw err;
        }
    };

    const signIn = async (email: string, password: string) => {
        if (!process.client || !$auth) throw new Error('Authentication not available');
        try {
            const userCredential = await signInWithEmailAndPassword($auth, email, password);
            user.value = userCredential.user;
            error.value = null;
            return userCredential.user;
        } catch (err: any) {
            error.value = err.message || 'Sign in failed';
            throw err;
        }
    };

    const signOutUser = async () => {
        if (!process.client || !$auth) throw new Error('Authentication not available');
        try {
            await signOut($auth);
            user.value = null;
            error.value = null;
        } catch (err: any) {
            error.value = err.message || 'Sign out failed';
            throw err;
        }
    };

    return {
        user,
        error,
        isLoading,
        initializeAuth,
        cleanupAuth,
        signUp,
        signIn,
        signOut: signOutUser,
    };
};