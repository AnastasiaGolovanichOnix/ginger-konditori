import { ref, computed } from 'vue';
import {
    addDoc,
    collection,
    deleteDoc,
    doc,
    getDocs,
    query,
    orderBy,
    where,
    startAfter,
    limit,
    updateDoc,
    serverTimestamp
} from 'firebase/firestore';
import {
    getStorage,
    ref as storageRef,
    uploadBytes,
    getDownloadURL
} from 'firebase/storage'; // Import Storage functions
import { useNuxtApp } from '#app';
import {MenuItemInterface} from "~/types/MenuItemInterface";

export default () => {
    const { $firestore, $storage } = useNuxtApp();

    const menu = ref<MenuItemInterface[]>([] as MenuItemInterface[]);
    const lastDoc = ref<any>(null);
    const totalItems = ref(0);

    const fetchTotalItems = async (filterStatus?: string, searchQuery?: string) => {
        let q = query(collection($firestore, 'menu'));

        if (filterStatus) {
            q = query(q, where('status', '==', filterStatus));
        }

        // Note: Firestore doesn't support full-text search natively
        const querySnapshot = await getDocs(q);
        let total = querySnapshot.size;

        if (searchQuery) {
            const filteredDocs = querySnapshot.docs.filter(doc =>
                doc.data().name.toLowerCase().includes(searchQuery.toLowerCase()) ||
                doc.data().category.toLowerCase().includes(searchQuery.toLowerCase())
            );
            total = filteredDocs.length; // Update total based on local filter
        }

        totalItems.value = total;
    };

    const getMenu = async (skip: number = 0, limitValue: number = 10, filterBy?: { field: string; value: any; operator?: '<' | '<=' | '==' | '>=' | '>' }) => {
        let q = query(
            collection($firestore, 'menu'),
            orderBy('createdAt', 'desc')
        );

        if (filterBy) {
            const { field, value, operator = '==' } = filterBy;
            q = query(q, where(field, operator, value));
        }

        if (skip > 0 && lastDoc.value) {
            q = query(q, startAfter(lastDoc.value));
        }

        q = query(q, limit(limitValue));

        const querySnapshot = await getDocs(q);

        if (!querySnapshot.empty) {
            lastDoc.value = querySnapshot.docs[querySnapshot.docs.length - 1];
        }

        menu.value = querySnapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data(),
            createdAt: doc.data().createdAt,
        })) as unknown as MenuItemInterface[];
    };

    const uploadImage = async (file: File): Promise<string> => {
        const storage = getStorage();
        const storageReference = storageRef(storage, `menu_images/${Date.now()}_${file.name}`);
        await uploadBytes(storageReference, file);
        const downloadURL = await getDownloadURL(storageReference);
        return downloadURL;
    };

    const addMenuItem = async (item: Omit<MenuItemInterface, 'id' | 'createdAt' | 'image'> & { imageFile?: File }) => {
        let imageUrl = '';
        if (item.imageFile) {
            // imageUrl = await uploadImage(item.imageFile);
        }

        console.log(imageUrl);
        const newItem = {
            name: item.name,
            image: imageUrl || '', // Use uploaded URL or empty string if no file
            category: item.category,
            price: item.price,
            status: item.status,
            createdAt: serverTimestamp(),
        };

        const docRef = await addDoc(collection($firestore, 'menu'), newItem);
        await getMenu();
    };

    const updateMenuItem = async (itemId: string, updates: Partial<MenuItemInterface> & { imageFile?: File }) => {
        let imageUrl = updates.image || '';
        if (updates.imageFile) {
            // imageUrl = await uploadImage(updates.imageFile);
        }

        const itemRef = doc($firestore, 'menu', itemId);
        const {imageFile, ...restUpdates} = updates;
        await updateDoc(itemRef, {
            ...restUpdates,
            ...(imageFile ? { image: imageUrl } : {}), // Update image only if a new file is provided
            updatedAt: serverTimestamp(), // Optional: add an updatedAt timestamp
        });
        await getMenu();
    };

    const deleteMenuItem = async (itemId: string) => {
        const itemRef = doc($firestore, 'menu', itemId);
        await deleteDoc(itemRef);
        await getMenu();
    };

    return {
        menu,
        totalItems,
        getMenu,
        fetchTotalItems,
        uploadImage,
        addMenuItem,
        updateMenuItem,
        deleteMenuItem,
    }
}