import { initializeApp } from 'firebase/app'
import { getAuth } from "firebase/auth"
import { getFirestore } from 'firebase/firestore'
import { getAnalytics } from "firebase/analytics"

export default defineNuxtPlugin(nuxtApp => {
    const config = useRuntimeConfig()

    const firebaseConfig = {
        apiKey: "AIzaSyCrPWyrMSLRreMusmuRJV2LI2hqYxhAGh0",
        authDomain: "vue-firebase-app-d0013.firebaseapp.com",
        projectId: "vue-firebase-app-d0013",
        storageBucket: "vue-firebase-app-d0013.firebasestorage.app",
        messagingSenderId: "750459710661",
        appId: "1:750459710661:web:f81558bac2c64959c554ac"
    };

    const app = initializeApp(firebaseConfig)

    const analytics = getAnalytics(app)
    const auth = getAuth(app)
    const firestore = getFirestore(app)

    nuxtApp.vueApp.provide('auth', auth)
    nuxtApp.provide('auth', auth)

    nuxtApp.vueApp.provide('firestore', firestore)
    nuxtApp.provide('firestore', firestore)
})