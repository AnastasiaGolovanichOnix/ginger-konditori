export interface MenuItemInterface {
    id: string;
    name: string;
    description: string;
    image: string;
    price: number;
    weight: number;
    category: string;
    status: string;
}