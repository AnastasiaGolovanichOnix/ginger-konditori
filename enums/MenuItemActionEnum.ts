export enum MenuItemActionEnum {
    VIEW = 'view',
    EDIT = 'edit',
    DELETE = 'delete',
    OUT_OF_STOCK = 'outOfStock'
}