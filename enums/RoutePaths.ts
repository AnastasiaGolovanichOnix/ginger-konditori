export enum RoutePaths {
    SIGN_IN = "/sign-in",
    SIGN_UP = '/sign-up',
    HOME = '/',
    ADMIN = '/admin',
}