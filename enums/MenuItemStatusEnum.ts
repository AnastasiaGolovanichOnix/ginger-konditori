export enum MenuItemStatusEnum {
    IN_STOCK = "in stock",
    OUT_OF_STOCK = "out of stock",
}